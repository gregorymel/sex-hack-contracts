var SHA256 = require("crypto-js/sha256");
const MedicalRegistry = artifacts.require("MedicalRegistry");

contract("MedicalRegistry", accounts => {
    it("should push data to registry", async () => {
        const instance = await MedicalRegistry.deployed();

        const userAcc = accounts[1];
        const doctorAcc = accounts[2];
        const data = "It's dummy medical data";
        const expectedHash = SHA256(data).toString()
        await instance.pushEntryHash(userAcc, expectedHash, { from: doctorAcc });

        const entry = await instance.getEntryHash.call(userAcc);
        const actualHash = entry[0];
        const addr = entry[1];

        assert.equal(
            expectedHash,
            actualHash,
            "Hash wasn't correctly taken from the registyr"
        );
        assert.equal(
            addr,
            doctorAcc,
            "It isn't doctor addr"
        );
    })
});