pragma solidity >=0.4.21 <0.7.0;

contract MedicalRegistry {
    address _onwer;

    struct Entry {
        string entryHash;
        address doctorAddr;
    }

    mapping (address => Entry) public _entries;

    constructor() public {
        _onwer = msg.sender;
    }

    function pushEntryHash(address user, string memory enrtyHash) public {
        _entries[user] = Entry({
            entryHash: enrtyHash,
            doctorAddr: msg.sender
        });
    }

    function getEntryHash(address user) public view returns(string memory, address) {
        Entry memory entry = _entries[user];

        return (entry.entryHash, entry.doctorAddr);
    }
}